//
//  AppDelegate.h
//  微信朋友圈位置
//
//  Created by chenleping on 2018/9/6.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

