//
//  NextViewController.m
//  微信朋友圈位置
//
//  Created by chenleping on 2018/9/6.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import "NextViewController.h"

@interface NextViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSInteger disappearTime;
    CGFloat _y;
}
@property (nonatomic, strong) UITableView * tableView;


@property (nonatomic, strong) NSTimer * timer;
@end

@implementation NextViewController

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    disappearTime = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        disappearTime ++;
        NSLog(@"disappearTime:---%lu", disappearTime);
        if (disappearTime == 2) {
            [self.tableView setContentOffset:CGPointMake(self.tableView.contentOffset.x, -_y) animated:YES];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.timer invalidate];
    self.timer = nil;
    CGFloat y = self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height;
    _y = y;
    NSLog(@"contentOffset.y:%f", self.tableView.contentOffset.y);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor greenColor];
    
    
    [self.view addSubview:self.tableView];
}

+ (instancetype)shareController{
    static NextViewController *nextVC;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nextVC = [[self alloc] init];
    });
    return nextVC;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellID = @"cellid";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"第%lu行...", indexPath.row];
    return cell;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    return _tableView;
}


@end
