//
//  NextViewController.h
//  微信朋友圈位置
//
//  Created by chenleping on 2018/9/6.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NextViewController : UIViewController

+ (instancetype)shareController;
// 让init、new方法失效，从而只能使用shareController方法
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end
