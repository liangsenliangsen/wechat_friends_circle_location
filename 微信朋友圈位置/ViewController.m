//
//  ViewController.m
//  微信朋友圈位置
//
//  Created by chenleping on 2018/9/6.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import "ViewController.h"

#import "NextViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor yellowColor];
    self.title = @"点击页面到下一页面";
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NextViewController * nextVC = [NextViewController shareController];
    [self.navigationController pushViewController:nextVC animated:YES];
}


@end
